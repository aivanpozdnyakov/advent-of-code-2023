# advent of code 2023
[Advent of Code](https://adventofcode.com/2023) is an annual set of Christmas-themed computer programming challenges.
Solved using modern C++20. 
`get_input` from `util.hxx` reads input directly from adventofcode.com using `curl`. However, this dependency is just a matter of convenience and completely optional. 
```bash
make day=1 
```