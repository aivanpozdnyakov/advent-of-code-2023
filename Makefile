CC=clang++
CCV=c++20 
# -fsanitize=undefined 
# -fsanitize=address -fno-omit-frame-pointer
# -fsanitize=address 
day: 
	@clear
	$(CC) -std=$(CCV) $(CFLAGS) src/day$(day).cpp -o out/day$(day) -lcurl
	@echo -----------
	./out/day$(day)
