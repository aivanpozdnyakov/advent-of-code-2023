#include "util.hxx"
#include <cstdio>
#include <sstream>
#include <cmath>

std::tuple<float, float> solve_eq(long time, long distance) {
    long D = time * time - 4 * distance;
    assert(D >= 0);
    return { (time + sqrtl(D)) / 2.0f, (time - sqrtl(D)) / 2.0f };
}

long part_0(const std::string &str) {
    auto lines = split(str, "\n");
    auto times = get_numbers(lines[0]);
    auto distances = get_numbers(lines[1]);
    long result = 1;
    for (int i = 0; i < times.size(); i++) {
        auto [a, b] = solve_eq(times[i], distances[i] + 1);
        if (b < 0)
            b = 0;
        result *= (floorf(a) - ceilf(b)) + 1;
    }
    return result;
}

long concat_numbers(const std::string &str) {
    auto matches = regex_loop(str, "(?:\\w?)(\\d+)");
    std::stringstream ss;
    for (auto match : matches) {
        ss << match[0];
    }
    return std::stol(ss.str());
}

long part_1(const std::string &str) {
    auto lines = split(str, "\n");
    long time = concat_numbers(lines[0]);
    long distance = concat_numbers(lines[1]);
    auto [a, b] = solve_eq(time, distance + 1);
    if (b < 0)
        b = 0;
    return (floorf(a) - ceilf(b)) + 1;
}

auto test_input_0 = R"(Time:      7  15   30
Distance:  9  40  200
)";

int main() {
    auto input_0 = get_input(6);
    ASSERT_EQ(part_0(test_input_0), 288);
    printf("Part 0 answer is: %ld\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 71503);
    printf("Part 1 answer is: %ld\n", part_1(input_0));
    return 0;
}
