#include "util.hxx"
#include <unordered_map>

int part_0(const std::string &str) {
    auto lines = split(str, "\n");
    auto rule = lines[0];
    std::unordered_map<std::string, std::pair<std::string, std::string>> map;
    for (int i = 1; i < lines.size(); i++) {
        auto line = lines[i];
        auto key = line.substr(0, 3);
        auto left = line.substr(7, 3);
        auto right = line.substr(12, 3);
        map[key] = { left, right };
    }
    std::string current = "AAA";
    int i = 0;
    for (i = 0; current != "ZZZ"; i++) {
        char c = rule[i % rule.size()];
        if (c == 'L') {
            current = map.at(current).first;
        } else {
            current = map.at(current).second;
        }
    }
    return i;
}

int part_1_solve(const std::unordered_map<std::string, std::pair<std::string, std::string>> map,
                 const std::string &rule,
                 const std::string &start) {
    std::string current = start;
    int i = 0;
    for (i = 0; current[2] != 'Z'; i++) {
        char c = rule[i % rule.size()];
        if (c == 'L') {
            current = map.at(current).first;
        } else {
            current = map.at(current).second;
        }
    }
    return i;
}

long long gcd(long long a, long long b) {
    long long tmp;
    while (b != 0) {
        tmp = b;
        b = a % b;
        a = tmp;
    }
    return a;
}

long long lcm(long long a, long long b) {
    return a * b / gcd(a, b);
}

long long part_1(const std::string &str) {
    auto lines = split(str, "\n");
    auto rule = lines[0];
    std::unordered_map<std::string, std::pair<std::string, std::string>> map;
    for (int i = 1; i < lines.size(); i++) {
        auto line = lines[i];
        auto key = line.substr(0, 3);
        auto left = line.substr(7, 3);
        auto right = line.substr(12, 3);
        map[key] = { left, right };
    }
    long long last_answer = 1;
    for (auto [key, _] : map) {
        if (key[2] == 'A') {
            last_answer = lcm(last_answer, part_1_solve(map, rule, key));
        }
    }
    return last_answer;
}

std::string test_input_0 = R"(RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
)";

std::string test_input_1 = R"(LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
)";

std::string test_input_2 = R"(LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
)";

int main() {
    auto input_0 = get_input(8);
    ASSERT_EQ(part_0(test_input_0), 2);
    ASSERT_EQ(part_0(test_input_1), 6);
    printf("Part 0 answer is: %d\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_2), 6);
    printf("Part 1 answer is: %lld\n", part_1(input_0)); // 14,935,034,899,483
    return 0;
}
