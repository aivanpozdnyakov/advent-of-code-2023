#include "util.hxx"
#include <unordered_map>
#include <algorithm>

int pow(int x, int rank) {
    int result = 1;
    for (; rank > 0; rank--) {
        result *= x;
    }
    return result;
}

std::unordered_map<char, int> scores = {
    { 'T', 10 },
    { 'J', 11 },
    { 'Q', 12 },
    { 'K', 13 },
    { 'A', 15 }
};

int count_values(const std::unordered_map<char, int> &map, int seeked) {
    int counter = 0;
    for (auto &[key, value] : map) {
        counter += value == seeked;
    }
    return counter;
}

enum HAND_TYPE {
    EMTPY,
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIR,
    THREE,
    FULL_HOUSE,
    FOUR,
    FIVE
};

HAND_TYPE determine_hand_type(std::unordered_map<char, int> map) {
    if (count_values(map, 5)) {
        return FIVE;
    }
    if (count_values(map, 4)) {
        return FOUR;
    }
    if (count_values(map, 3) && count_values(map, 2))
        return FULL_HOUSE;
    if (count_values(map, 3)) {
        return THREE;
    }
    if (count_values(map, 2) == 2) {
        return TWO_PAIR;
    }
    if (count_values(map, 2) == 1) {
        return ONE_PAIR;
    }
    if (count_values(map, 1)) {
        return HIGH_CARD;
    }
    return EMTPY;
}

int part_0(const std::string &str) {
    std::vector<std::tuple<std::string, int, int>> cards;
    std::unordered_map<char, int> map;
    for (auto line : split(str, "\n")) {
        auto card = line.substr(0, 5);
        int bid = std::stoi(line.substr(6));
        int rank = 5;
        int score = 0;
        for (char c : card) {
            map[c]++;
            int c_score = scores[c];
            if (c_score == 0) {
                c_score = c - '0';
            }
            score += c_score * pow(16, rank--);
        }
        score += determine_hand_type(map) * pow(16, 6);
        cards.push_back({ card, bid, score });
        map.clear();
    }
    std::sort(cards.begin(), cards.end(), [](auto &x, auto &y) {
        return std::get<2>(x) < std::get<2>(y);
    });
    int result = 0;
    int ind = 0;
    for (auto [card, bid, rank] : cards) {
        result += bid * ++ind;
    }
    return result;
}

void improve_hand_type(HAND_TYPE &type) {
    switch (type) {
    case FOUR:
        type = FIVE;
        break;
    case THREE:
    case FULL_HOUSE:
        type = FOUR;
        break;
    case ONE_PAIR:
        type = THREE;
        break;
    case TWO_PAIR:
        type = FULL_HOUSE;
        break;
    case HIGH_CARD:
        type = ONE_PAIR;
        break;
    case FIVE:
        break;
    case EMTPY:
        type = HIGH_CARD;
        break;
    }
}

int part_1(const std::string &str) {
    std::vector<std::tuple<std::string, int, long long>> cards;
    std::unordered_map<char, int> map;
    for (auto line : split(str, "\n")) {
        auto card = line.substr(0, 5);
        int bid = std::stoi(line.substr(6));
        int rank = 5;
        long long score = 0;
        int jokers_count = 0;
        for (char c : card) {
            if (c == 'J') {
                jokers_count++;
                score += pow(16, rank--);
                continue;
            }
            int c_score = scores[c];
            if (c_score == 0) {
                c_score = c - '0';
            }
            score += c_score * pow(16, rank--);
            map[c]++;
        }
        HAND_TYPE type = determine_hand_type(map);
        for (int i = 0; i < jokers_count; i++) {
            improve_hand_type(type);
        }
        assert(type != EMTPY);
        score += type * pow(16, 6);
        cards.push_back({ card, bid, score });
        map.clear();
    }
    std::sort(cards.begin(), cards.end(), [](auto &x, auto &y) {
        return std::get<2>(x) < std::get<2>(y);
    });
    int result = 0;
    int ind = 0;
    for (auto [card, bid, rank] : cards) {
        result += bid * ++ind;
    }
    return result;
}

std::string test_input_0 = R"(32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
)";

int main() {
    auto input_0 = get_input(7);
    ASSERT_EQ(part_0(test_input_0), 6440);
    printf("Part 0 answer is: %d\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 5905);
    printf("Part 1 answer is: %d\n", part_1(input_0)); // 250359386
    return 0;
}
