#include "util.hxx"
#include <vector>

long interpolate_last_element(std::vector<long> &numbers) {
    bool is_all_zeroes = true;
    std::vector<long> new_numbers(numbers.size() - 1);
    for (long i = 0; i < numbers.size() - 1; i++) {
        new_numbers[i] = numbers[i + 1] - numbers[i];
        is_all_zeroes &= new_numbers[i] == 0;
    }
    return numbers[numbers.size() - 1] +
           (is_all_zeroes ? 0 : interpolate_last_element(new_numbers));
}

long long part_0(const std::string &str) {
    long long sum = 0;
    for (auto &line : split(str, "\n")) {
        auto numbers = get_numbers(line);
        sum += interpolate_last_element(numbers);
    }
    return sum;
}

long interpolate_first_element(std::vector<long> &numbers) {
    bool is_all_zeroes = true;
    std::vector<long> new_numbers(numbers.size() - 1);
    for (long i = 0; i < numbers.size() - 1; i++) {
        new_numbers[i] = numbers[i + 1] - numbers[i];
        is_all_zeroes &= new_numbers[i] == 0;
    }
    return numbers[0] -
           (is_all_zeroes ? 0 : interpolate_first_element(new_numbers));
}

long long part_1(const std::string &str) {
    long long sum = 0;
    for (auto &line : split(str, "\n")) {
        auto numbers = get_numbers(line);
        sum += interpolate_first_element(numbers);
    }
    return sum;
}

const char *test_input_0 = R"(0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
)";

int main() {
    std::cout << get_numbers("-159")[0] << std::endl;
    auto input_0 = get_input(9);
    ASSERT_EQ(part_0(test_input_0), 114);
    printf("Part 0 answer is: %lld\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 2);
    printf("Part 1 answer is: %lld\n", part_1(input_0));
    return 0;
}
