#include <iostream>
#include <string>
#include <curl/curl.h>
#include <regex>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <cassert>

using namespace std::string_literals;

static size_t write_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *)userp)->append((char *)contents, size * nmemb);
    return size * nmemb;
}

inline std::string get_input(int day) {
    /* reads input data from curl, or from file if already cached */
    const std::string file_name = "input/"s + std::to_string(day);
    std::ifstream in_file(file_name);
    if (in_file.is_open()) {
        printf("Reading input from `%s`\n", file_name.c_str());
        std::stringstream buffer;
        buffer << in_file.rdbuf();
        in_file.close();
        return buffer.str();
    }
    CURL *curl;
    CURLcode res;
    std::string read_buffer;
    curl = curl_easy_init();
    if (!curl) {
        std::cout << "ASSERTION ERROR >> Curl is not properly initialized\n";
        exit(1);
    }
    curl_easy_setopt(curl, CURLOPT_COOKIE, "session=53616c7465645f5ffe87037d4198bebf92d2b0d8db7e4d1e5b1d0c6b06d4f13cc9dc179f38cbaf6f107f2fff000e242fea00e50c8d0616f80dc07487de012622;");
    std::string url = "https://adventofcode.com/2023/day/" + std::to_string(day) + "/input";
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &read_buffer);
    printf("Reading input from `%s`\n", url.c_str());
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    std::fstream out_file(file_name, std::ios::out);
    assert(out_file.is_open());
    out_file << read_buffer;
    out_file.close();
    return read_buffer;
}

inline bool contains(std::string_view str, char c) {
    return str.find(c) != std::string::npos;
}

inline std::vector<std::string> split(const std::string &str, std::string_view delim) {
    size_t ind = 0;
    std::vector<std::string> result;
    while (1) {
        auto new_ind = str.find_first_of(delim, ind);
        if (new_ind == std::string::npos) {
            if (!contains(delim, str[ind]) && str.size() - ind > 0)
                result.push_back(str.substr(ind));
            return result;
        }
        if (!contains(delim, str[ind]))
            result.push_back(str.substr(ind, new_ind - ind));
        ind = new_ind + 1;
    }
}

inline std::vector<std::smatch> regex_loop(const std::string &str, const std::string &pattern) {
    std::vector<std::smatch> result;
    std::regex pattern_regex(pattern);
    auto matches_begin =
        std::sregex_iterator(str.begin(), str.end(), pattern_regex);
    auto matches_end = std::sregex_iterator();
    for (std::sregex_iterator i = matches_begin; i != matches_end; ++i) {
        result.push_back(*i);
    }
    return result;
}

inline std::vector<long> get_numbers(const std::string &str) {
    auto matches = regex_loop(str, "(?:\\w?)((-?)\\d+)");
    std::vector<long> numbers;
    numbers.reserve(matches.size());
    for (auto match : matches) {
        numbers.push_back(std::stol(match[0]));
    }
    return numbers;
}

inline int sign(int v) {
    if (v == 0)
        return v;
    return v > 0 ? 1 : -1;
}

inline long sign(long v) {
    if (v == 0)
        return v;
    return v > 0 ? 1 : -1;
}

#define ASSERT_EQ(x, y)                                                                            \
    {                                                                                              \
        auto val = x;                                                                              \
        auto expected = y;                                                                         \
        if (val != expected) {                                                                     \
            std::cout << "ASSERTION ERROR >> expected `" << expected << "` got `" << val << "`\n"; \
            exit(1);                                                                               \
        }                                                                                          \
    }
