#include "util.hxx"
#include <array>
#include <stdio.h>

            int part_0(const std::string &str) {
    int sum = 0;
    int first_digit = 0;
    int last_digit = 0;
    for (char c : str) {
        if (c == '\n') {
            sum += first_digit * 10 + last_digit;
            first_digit = 0;
            last_digit = 0;
        }
        if (!isdigit(c))
            continue;
        last_digit = c - '0';
        if (first_digit == 0)
            first_digit = last_digit;
    }
    return sum;
}

std::array<std::string, 9> digits_as_str = { {
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
} };

int part_1(const std::string &str) {
    int sum = 0;
    int first_digit = 0;
    int last_digit = 0;
    for (int i = 0; i < str.size(); i++) {
        char c = str[i];
        if (c == '\n') {
            sum += first_digit * 10 + last_digit;
            first_digit = 0;
            last_digit = 0;
        }
        for (int j = 0; j < digits_as_str.size(); j++) {
            auto digit_as_str = digits_as_str[j];
            if (str.substr(i, digit_as_str.length()) == digit_as_str) {
                last_digit = j + 1;
                if (first_digit == 0)
                    first_digit = last_digit;
                break;
            }
        }
        if (!isdigit(c))
            continue;
        last_digit = c - '0';
        if (first_digit == 0)
            first_digit = last_digit;
    }
    return sum;
}

const char *test_input_0 = R"(1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
)";

const char *test_input_1 = R"(two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
)";

/*
On each line, the calibration value can be found by combining the first digit and the last digit (in that order)
to form a single two-digit number.
part 0: What is the sum of all of the calibration values?
part 1: It looks like some of the digits are actually spelled out with letters:
        one, two, three, four, five, six, seven, eight, and nine also count as valid "digits"
        What is the sum of all of the calibration values?
*/

int main() {
    ASSERT_EQ(part_0(test_input_0), 142);
    auto input_0 = get_input(1);
    printf("Part 0 answer is: %d\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_1), 281);
    printf("Part 1 answer is: %d\n", part_1(input_0));
    return 0;
}
