#include "util.hxx"
#include <cassert>
#include <unordered_map>
#include <stdio.h>
#include <unordered_set>
#include <vector>

int part_0(const std::string & str){
    int result = 0;
    for (auto & line : split(str, "\n")){
        int score = 0;
        auto split_line = split(line, ":|");
        assert(split_line.size() == 3);
        std::unordered_set<std::string> winning;
        for (auto match : regex_loop(split_line[2], "(?:\\w?)(\\d)")){
            winning.insert(match[0]);
        }
        for (auto match : regex_loop(split_line[1], "(?:\\w?)(\\d)")){
            if (winning.contains(match[0])){
                score = score == 0 ? 1 : score * 2;
            }
        }
        result += score;
    }
    return result;
}

int part_1(const std::string & str){
    auto lines = split(str, "\n");
    std::vector<int> buffer(lines.size(), 1);
    int line_ind = 0;
    for (auto & line : lines){
        int score = 0;
        auto split_line = split(line, ":|");
        assert(split_line.size() == 3);
        std::unordered_set<std::string> winning;
        for (auto match : regex_loop(split_line[2], "(?:\\w?)(\\d)")){
            winning.insert(match[0]);
        }
        for (auto match : regex_loop(split_line[1], "(?:\\w?)(\\d)")){
            score += winning.contains(match[0]);
        }
        for (int offset = 0; offset < score; offset++){
            if (line_ind + offset + 1 >= buffer.size()) break;
            buffer[line_ind + offset + 1] += buffer[line_ind];
        }
        line_ind++;
    }
    int result = 0;
    for (int & num : buffer){
        result += num;
    }
    return result;
}

const char *test_input_0 = R"(Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
)";

int main(){
    ASSERT_EQ(part_0(test_input_0), 13);
    auto input_0 = get_input(4);
    printf("Part 0 answer is: %d\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 30);
    printf("Part 1 answer is: %d\n", part_1(input_0));
    return 0;
}