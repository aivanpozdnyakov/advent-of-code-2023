#include "util.hxx"
#include <stdio.h>

int part_0(const std::string &str) {
    std::string pattern = R"((\d+)\s(blue|red|green)(?:[,;\s\n]?))";
    int game_id = 0;
    int result = 0;
    for (auto &line : split(str, "\n")) {
        int red = 0, green = 0, blue = 0;
        for (auto &match : regex_loop(line, pattern)) {
            int value = std::strtol(match[1].str().c_str(), NULL, 10);
            std::string color = match[2].str();
            int &existing_val = (color == "red" ? red : (color == "green" ? green : blue));
            if (value > existing_val)
                existing_val = value;
        }
        game_id++;
        if (red <= 12 && green <= 13 && blue <= 14)
            result += game_id;
    }
    return result;
}

long part_1(const std::string &str) {
    std::string pattern = R"((\d+)\s(blue|red|green)(?:[,;\s\n]?))";
    long result = 0;
    for (auto &line : split(str, "\n")) {
        int red = 0, green = 0, blue = 0;
        for (auto &match : regex_loop(line, pattern)) {
            int value = std::strtol(match[1].str().c_str(), NULL, 10);
            std::string color = match[2].str();
            int &existing_val = (color == "red" ? red : (color == "green" ? green : blue));
            if (value > existing_val)
                existing_val = value;
        }
        result += red * green * blue;
    }
    return result;
}

const char *test_input_0 = R"(Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
)";

/*
Determine which games would have been possible if the bag had been loaded
with only 12 red cubes, 13 green cubes, and 14 blue cubes.
What is the sum of the IDs of those games?
*/

int main() {
    ASSERT_EQ(part_0(test_input_0), 8);
    auto input_0 = get_input(2);
    printf("Part 0 answer is: %d\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 2286);
    printf("Part 1 answer is: %ld\n", part_1(input_0));
    return 0;
}