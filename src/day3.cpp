#include "util.hxx"
#include <unordered_map>
#include <stdio.h>

bool is_asterisk(const char *asterisk_ptr, const std::string &str) {
    const char *start = &str.data()[0];
    return asterisk_ptr < start + str.size() &&
           asterisk_ptr >= start &&
           !isdigit(*asterisk_ptr) && *asterisk_ptr != '.' && *asterisk_ptr != '\n';
}

int part_0(const std::string &str) {
    int result = 0;
    int line_len = str.find('\n') + 1;
    int line_ind = 0;
    const char *start = &str[0];
    for (const char *c_ptr = start; c_ptr < start + str.size(); c_ptr++) {
        char c = *c_ptr;
        if (c == '\n') {
            line_ind++;
        }
        if (!isdigit(c))
            continue;
        char *num_end = nullptr;
        int number = strtol(c_ptr, &num_end, 10);
        for (int y = -1; y <= 1; y++) {
            for (const char *xptr = c_ptr - 1; xptr <= num_end; xptr++) {
                const char *asterisk_ptr = xptr + y * line_len;
                if (is_asterisk(asterisk_ptr, str)) {
                    result += number;
                    y = 1;
                    break;
                }
            }
        }
        c_ptr = num_end;
    }
    return result;
}

int part_1(const std::string &str) {
    int result = 0;
    int line_len = str.find('\n') + 1;
    int line_ind = 0;
    const char *start = &str.data()[0];
    std::unordered_map<const char *, std::pair<int, int>> map;
    for (const char *c_ptr = start; c_ptr < start + str.size(); c_ptr++) {
        char c = *c_ptr;
        if (c == '\n') {
            line_ind++;
        }
        if (!isdigit(c))
            continue;
        char *num_end = nullptr;
        int number = strtol(c_ptr, &num_end, 10);
        for (int y = -1; y <= 1; y++) {
            for (const char *xptr = c_ptr - 1; xptr <= num_end; xptr++) {
                const char *asterisk_ptr = xptr + y * line_len;
                if (!is_asterisk(asterisk_ptr, str))
                    continue;
                if (map[asterisk_ptr] == std::pair{ 0, 0 }) {
                    map[asterisk_ptr] = { number, 0 };
                } else if (map[asterisk_ptr].second == 0) {
                    map[asterisk_ptr].second = number;
                } else {
                    map[asterisk_ptr].first = 0;
                }
                y = 1;
                break;
            }
        }
        c_ptr = num_end;
    }
    for (auto &[_, pair] : map) {
        result += pair.first * pair.second;
    }
    return result;
}

const char *test_input_0 = R"(467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
)";

/*
Part 0: Any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum
Part 1: A gear is any * symbol that is adjacent to exactly two part numbers.
        Its gear ratio is the result of multiplying those two numbers together.
        You need to find the gear ratio of every gear and add them all up.
*/

int main() {
    ASSERT_EQ(part_0(test_input_0), 4361);
    auto input_0 = get_input(3);
    printf("Part 0 answer is: %d\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 467835);
    printf("Part 1 answer is: %d\n", part_1(input_0));
    return 0;
}