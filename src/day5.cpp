#include "util.hxx"
#include <algorithm>
#include <cstdio>
#include <vector>

struct Range {
    long start = 0;
    long end = 0; // non inclusive, e.g. range [start, end)
    long delta = 0;
    Range &apply_delta() {
        start += delta;
        end += delta;
        delta = 0;
        return *this;
    }
};

void transform_seeds(long &seed, const std::vector<Range> &ranges) {
    for (const auto &range : ranges) {
        if (seed >= range.start && seed < range.end) {
            seed += range.delta;
            return;
        }
    }
}

long part_0(const std::string &str) {
    long result = 0;
    auto lines = split(str, "\n");
    auto seeds = get_numbers(lines[0]);
    std::vector<Range> ranges;
    for (int i = 2; i < lines.size(); i++) {
        auto line = lines[i];
        if (isdigit(line[0])) {
            auto numbers = get_numbers(line);
            ASSERT_EQ(numbers.size(), 3);
            long dest = numbers[0];
            long src = numbers[1];
            long len = numbers[2];
            ranges.push_back({ src, src + len, dest - src });
        } else {
            for (auto &seed : seeds) {
                transform_seeds(seed, ranges);
            }
            ranges.clear();
        }
    }
    for (auto &seed : seeds) {
        transform_seeds(seed, ranges);
    }
    return *std::min_element(seeds.cbegin(), seeds.cend());
}

void transform_seed_ranges(Range &s, std::vector<Range> &seeds, const std::vector<Range> &ranges) {
    for (const auto &r : ranges) {
        if (s.start >= r.start && s.end <= r.end) {
            /* |-----range-----|
                    |-seed-|         */
            s.delta = r.delta;
            s.apply_delta();
            return;
        }
        if (s.start <= r.start && s.end >= r.end) {
            /* |------seed-----|
                    |-range-|         */
            auto new_seed_1 = Range{ s.start, r.start };
            auto new_seed_2 = Range{ r.end, s.end };
            s = r;
            s.apply_delta();
            seeds.push_back(new_seed_1);
            seeds.push_back(new_seed_2);
            return;
        }
        if (s.start >= r.start && s.start < r.end) {
            /*    |---seed---|
              |---range---|            */
            auto new_seed = Range{ r.end, s.end };
            s = { s.start, r.end, r.delta };
            s.apply_delta();
            seeds.push_back(new_seed);
            return;
        }
        if (s.start <= r.start && r.start < s.end) {
            /*      |---range---|
                |---seed---|  */
            Range new_seed = { s.start, r.start };
            s = { r.start, s.end, r.delta };
            s.apply_delta();
            seeds.push_back(new_seed);
            return;
        }
    }
    s.apply_delta();
}

long part_1(const std::string &str) {
    long result = 0;
    auto lines = split(str, "\n");
    auto seed_numbers = get_numbers(lines[0]);
    std::vector<Range> seeds;
    for (int i = 0; i < seed_numbers.size(); i += 2) {
        long src = seed_numbers[i];
        long len = seed_numbers[i + 1];
        seeds.push_back({ src, src + len });
    }
    std::vector<Range> ranges;
    for (int i = 2; i < lines.size(); i++) {
        auto line = lines[i];
        if (isdigit(line[0])) {
            auto numbers = get_numbers(line);
            ASSERT_EQ(numbers.size(), 3);
            long dest = numbers[0];
            long src = numbers[1];
            long len = numbers[2];
            ranges.push_back({ src, src + len, dest - src });
        } else {
            for (int seed_id = 0; seed_id < seeds.size(); seed_id++) {
                auto &seed = seeds[seed_id];
                transform_seed_ranges(seed, seeds, ranges);
            }
            ranges.clear();
        }
    }
    long min = LONG_MAX;
    for (int seed_id = 0; seed_id < seeds.size(); seed_id++) {
        auto &seed = seeds[seed_id];
        transform_seed_ranges(seed, seeds, ranges);
        if (seeds[seed_id].start < min)
            min = seeds[seed_id].start;
    }
    return min;
}

auto test_input_0 = R"(seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
)";

int main() {
    auto input_0 = get_input(5);
    ASSERT_EQ(part_0(test_input_0), 35);
    printf("Part 0 answer is: %ld\n", part_0(input_0));
    ASSERT_EQ(part_1(test_input_0), 46);
    printf("Part 1 answer is: %ld\n", part_1(input_0));
    return 0;
}
